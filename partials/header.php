<!doctype html>
<html class="no-js" >
<head>
    <meta charset="utf-8">
    <title>Baudartius</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/jquery.fancybox-thumbs.css">

    <link rel="stylesheet" href="css/fullcalendar.css">
    <link rel="stylesheet" href="css/main.css">


    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true&.js&libraries=places&language=en"></script>

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/vendor/gmaps.js"></script>

</head>
<body>



<button class="nav">
    <span class="icon-container">
        <span class="line line01"></span>
        <span class="line line02"></span>
        <span class="line line03"></span>
        <span class="line line04"></span>
        <span class="line line05"></span>
        <span class="line line06"></span>
    </span>
</button>



<div class="body-overlay"></div><!--Overlay-->


<!--Navigation-->
<nav class="main-navigation-wrapper">

    <div class="main-navigation-inner">
        <a class="search" id='trigger-open' href="javascript:void(0)">
            <i class="icon-search"></i>
        </a>

        <form action="#" method="GET" id="search-form">
            <input type="text" placeholder="Search..." autofocus="autofocus">
        </form>


        <ul id="menu-primary-menu" class="menu"><li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="http://baudartius.sprigslab.com/news/">Nieuws</a></li>
            <li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-132"><i class="icon-subnavigation"></i><a href="#">Over ons</a>
                <ul class="sub-menu">
                    <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-58"><i class="icon-subnavigation"></i><a href="#">ONZE SCHOOL</a>
                        <ul class="sub-menu">
                            <li id="menu-item-382" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-382"><a href="http://baudartius.sprigslab.com/welkom/">Welkom</a></li>
                            <li id="menu-item-820" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-820"><a href="http://baudartius.sprigslab.com/onze-schoolgids/">Onze schoolgids</a></li>
                            <li id="menu-item-812" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-812"><a href="http://baudartius.sprigslab.com/ons-schoolplan/">Ons schoolplan</a></li>
                            <li id="menu-item-699" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-699"><a href="http://baudartius.sprigslab.com/schoolplan-en-schoolgids/">Onze leerling</a></li>
                            <li id="menu-item-356" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-356"><a href="http://baudartius.sprigslab.com/onze-collega/">Onze collega</a></li>
                            <li id="menu-item-826" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-826"><a href="http://baudartius.sprigslab.com/onze-rector/">Onze rector</a></li>
                            <li id="menu-item-357" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-357"><a href="http://baudartius.sprigslab.com/ons-logo/">Ons logo</a></li>
                            <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-360"><a href="http://baudartius.sprigslab.com/ons-bestuur/">Ons bestuur</a></li>
                            <li id="menu-item-747" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-747"><a href="http://baudartius.sprigslab.com/onze-resultaten/">Onze resultaten</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-327" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-327"><i class="icon-subnavigation"></i><a href="#">ONS ONDERWIJS</a>
                        <ul class="sub-menu">
                            <li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-371"><a href="http://baudartius.sprigslab.com/wat-is-jouw-niveau/">Wat is jouw niveau?</a></li>
                            <li id="menu-item-337" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-337"><a href="http://baudartius.sprigslab.com/mavo/">MAVO</a></li>
                            <li id="menu-item-336" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-336"><a href="http://baudartius.sprigslab.com/havo/">HAVO</a></li>
                            <li id="menu-item-335" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-335"><a href="http://baudartius.sprigslab.com/atheneum/">Atheneum</a></li>
                            <li id="menu-item-334" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-334"><a href="http://baudartius.sprigslab.com/gymnasium/">Gymnasium</a></li>
                            <li id="menu-item-333" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-333"><a href="http://baudartius.sprigslab.com/cambridge-engels/">Cambridge Engels</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-141" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-141"><i class="icon-subnavigation"></i><a href="#">Onze visie</a>
                        <ul class="sub-menu">
                            <li id="menu-item-381" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-381"><a href="http://baudartius.sprigslab.com/visie/">Visie</a></li>
                            <li id="menu-item-380" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-380"><a href="http://baudartius.sprigslab.com/missie/">Onze missie</a></li>
                            <li id="menu-item-379" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-379"><a href="http://baudartius.sprigslab.com/onze-kernwaarden/">Onze kernwaarden</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-142" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-142"><i class="icon-subnavigation"></i><a href="#">Onze geschiedenis</a>
                        <ul class="sub-menu">
                            <li id="menu-item-359" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-359"><a href="http://baudartius.sprigslab.com/onze-geschiedenis/">Wie is Billy?</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-127" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-127"><i class="icon-subnavigation"></i><a href="#">Onze app</a>
                        <ul class="sub-menu">
                            <li id="menu-item-391" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-391"><a href="http://baudartius.sprigslab.com/de-baudartius-app/">Onze Baudartius app</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-164" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-164"><i class="icon-subnavigation"></i><a href="#">Schoolkosten</a>
                        <ul class="sub-menu">
                            <li id="menu-item-412" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-412"><a href="http://baudartius.sprigslab.com/info-schoolkosten/">Info schoolkosten</a></li>
                            <li id="menu-item-411" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-411"><a href="http://baudartius.sprigslab.com/ouderbijdrage/">Ouderbijdrage</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-163"><i class="icon-subnavigation"></i><a href="#">Zorg en begeleiding</a>
                        <ul class="sub-menu">
                            <li id="menu-item-410" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-410"><a href="http://baudartius.sprigslab.com/intro-zorg-en-begeleiding/">Intro zorg en begeleiding</a></li>
                            <li id="menu-item-409" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-409"><a href="http://baudartius.sprigslab.com/dyslexie/">Dyslexie</a></li>
                            <li id="menu-item-408" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-408"><a href="http://baudartius.sprigslab.com/trainingen/">Trainingen</a></li>
                            <li id="menu-item-407" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-407"><a href="http://baudartius.sprigslab.com/extern/">Extern</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-162"><i class="icon-subnavigation"></i><a href="#">Studieloopbaan</a>
                        <ul class="sub-menu">
                            <li id="menu-item-631" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-631"><a href="http://baudartius.sprigslab.com/studeren-na-je-diploma/">Studeren na je diploma</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-435" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-435"><i class="icon-subnavigation"></i><a href="#">Maatschappelijke stage</a>
                        <ul class="sub-menu">
                            <li id="menu-item-433" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-433"><a href="http://baudartius.sprigslab.com/waarom-stage/">Waarom stage?</a></li>
                            <li id="menu-item-432" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-432"><a href="http://baudartius.sprigslab.com/leerlingen-op-stage/">Leerlingen op stage</a></li>
                            <li id="menu-item-431" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-431"><a href="http://baudartius.sprigslab.com/ouderinformatie-over-stages/">Ouderinformatie over stages</a></li>
                            <li id="menu-item-430" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-430"><a href="http://baudartius.sprigslab.com/heeft-u-een-stage-plek/">Heeft u een stage plek?</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-436" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-436"><i class="icon-subnavigation"></i><a href="#">Baudartius digitaal</a>
                        <ul class="sub-menu">
                            <li id="menu-item-429" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-429"><a href="http://baudartius.sprigslab.com/digitaal-leren-en-werken/">Digitaal leren en werken</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-192"><i class="icon-subnavigation"></i><a href="#">Onze activiteiten</a>
                        <ul class="sub-menu">
                            <li id="menu-item-451" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-451"><a href="http://baudartius.sprigslab.com/artistiek/">Artistiek</a></li>
                            <li id="menu-item-450" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-450"><a href="http://baudartius.sprigslab.com/bc-sport/">BC sport</a></li>
                            <li id="menu-item-449" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-449"><a href="http://baudartius.sprigslab.com/wetenschappelijk/">Wetenschappelijk</a></li>
                            <li id="menu-item-448" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-448"><a href="http://baudartius.sprigslab.com/maatschappelijk/">Maatschappelijk</a></li>
                            <li id="menu-item-447" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-447"><a href="http://baudartius.sprigslab.com/internationaal/">Internationaal</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-209" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-209"><i class="icon-subnavigation"></i><a href="#">Roosters</a>
                        <ul class="sub-menu">
                            <li id="menu-item-463" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-463"><a href="http://baudartius.sprigslab.com/jaarrooster/">Jaarrooster</a></li>
                            <li id="menu-item-462" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-462"><a href="http://baudartius.sprigslab.com/lestijden/">Lestijden</a></li>
                            <li id="menu-item-461" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-461"><a href="http://baudartius.sprigslab.com/vakantierooster/">Vakanties 16/17</a></li>
                            <li id="menu-item-460" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-460"><a href="http://baudartius.sprigslab.com/organisatiedagen/">Organisatiedagen</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-56"><i class="icon-subnavigation"></i><a href="#">Groep 8</a>
                <ul class="sub-menu">
                    <li id="menu-item-233" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-233"><i class="icon-subnavigation"></i><a href="#">Brugklas</a>
                        <ul class="sub-menu">
                            <li id="menu-item-516" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-516"><a href="http://baudartius.sprigslab.com/info-brugklas/">Info brugklas</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-232" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-232"><i class="icon-subnavigation"></i><a href="#">Wat kies jij?</a>
                        <ul class="sub-menu">
                            <li id="menu-item-488" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-488"><a href="http://baudartius.sprigslab.com/bc-lijn/">BC-lijn</a></li>
                            <li id="menu-item-489" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-489"><a href="http://baudartius.sprigslab.com/wat-kies-jij/">KC lijn</a></li>
                            <li id="menu-item-487" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-487"><a href="http://baudartius.sprigslab.com/sport-lijn/">Sport lijn</a></li>
                            <li id="menu-item-486" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-486"><a href="http://baudartius.sprigslab.com/tn-lijn/">TN lijn</a></li>
                            <li id="menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-485"><a href="http://baudartius.sprigslab.com/beta-excellent/">Bèta excellent</a></li>
                            <li id="menu-item-484" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-484"><a href="http://baudartius.sprigslab.com/gymnasium-2/">Gymnasium</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-242" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-242"><i class="icon-subnavigation"></i><a href="#">Kom kijken</a>
                        <ul class="sub-menu">
                            <li id="menu-item-483" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-483"><a href="http://baudartius.sprigslab.com/open-dag/">Open dag</a></li>
                            <li id="menu-item-482" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-482"><a href="http://baudartius.sprigslab.com/meeloopavonden/">Meeloopavonden</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-57"><i class="icon-subnavigation"></i><a href="#">Contact</a>
                <ul class="sub-menu">
                    <li id="menu-item-261" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-261"><i class="icon-subnavigation"></i><a href="#">Aanmelden</a>
                        <ul class="sub-menu">
                            <li id="menu-item-511" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-511"><a href="http://baudartius.sprigslab.com/toelatingsbeleid/">Toelatingsbeleid</a></li>
                            <li id="menu-item-510" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-510"><a href="http://baudartius.sprigslab.com/eerste-jaars/">Eerste jaars</a></li>
                            <li id="menu-item-509" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-509"><a href="http://baudartius.sprigslab.com/instromers/">Instromers</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-262"><i class="icon-subnavigation"></i><a href="#">Ziek-beter-vrij</a>
                        <ul class="sub-menu">
                            <li id="menu-item-513" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-513"><a href="http://baudartius.sprigslab.com/betermelden-isendoornstraat/">Betermelden Isendoornstraat</a></li>
                            <li id="menu-item-512" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-512"><a href="http://baudartius.sprigslab.com/betermelden-berkenlaan/">Betermelden Berkenlaan</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-263" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-263"><i class="icon-subnavigation"></i><a href="#">Onze gebouwen</a>
                        <ul class="sub-menu">
                            <li id="menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-515"><a href="http://baudartius.sprigslab.com/gebouw-isendoornstraat-22/">Gebouw Isendoornstraat</a></li>
                            <li id="menu-item-514" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-514"><a href="http://baudartius.sprigslab.com/gebouw-berkenlaan/">Gebouw Berkenlaan</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
</nav>


<header class="container">
    <div class="header-inner"> <!-- if you want sticky header please add class .sticky-header -->
        <div class="header-logo">
            <a href="index.php">
                <img src="img/logo.svg" alt="Baudartius">
            </a>
        </div>
    </div>
</header>



<div class="main-content">
    <div class="nav-right-wrap">
        <div class="nav-right-wrap-inner">
            <ul class="right-nav">
                <li>
                    <a href="#"><img class="sidebar-icon costum-icon-phone" src="img/icons/home.svg" alt=""></a>
                </li>
                <li>
                    <a href="#"><img class="sidebar-icon costum-icon-phone" src="img/icons/phone.svg" alt=""></a>
                </li>
                <li>
                    <a href="#"><img class="sidebar-icon costum-icon-calendar" src="img/icons/calendar.svg" alt=""></a>
                </li>
                <li>
                    <a href="#"><img class="sidebar-icon costum-icon-gallery" src="img/icons/gallery.svg" alt=""></a>
                </li>
                <li class="has-dropdown">
                    <a href="javascript:void(0)" id="trigger-webmail">
                        <img class="sidebar-icon costum-icon-webmail" src="img/icons/magister.svg" alt="">
                    </a>
                    <ul class="sub-nav" id="sub-webmail" style="width: 219px">
                        <li>
                            <a href="https://baudartius.swp.nl/5.7.4.2/magister.aspx">
                                <img class="sidebar-icon costum-icon-webmail" src="img/icons/webmail.svg" alt="">
                            </a>
                            </a>
                        </li>

                        <li><a href="https://baudartius.magister.net/#/inloggen">
                                <img class="sidebar-icon costum-icon-webmail" src="img/icons/leerar-op.svg" alt="">
                            </a>
                        </li>

                        <li><a href="https://baudartius.magister.net/#/inloggen">
                                <img class="sidebar-icon costum-icon-webmail" src="img/icons/leering.svg" alt="">
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="has-dropdown">
                    <a href="javascript:void(0)" id="trigger-social"><img class="sidebar-icon costum-icon-share" src="img/icons/share.svg" alt=""></a>
                    <ul class="sub-nav" style="width:293px" id="social-icons">
                        <li><a href="https://www.youtube.com/user/BaudartiusCollege"><img class="sidebar-icon" src="img/icons/bau-youtube.svg" alt=""></a></li>
                        <li><a href="https://www.facebook.com/Baudartius/"><img class="sidebar-icon" src="img/icons/bau-facebook.svg" alt=""></a></li>
                        <li><a href="https://www.instagram.com/baudartiuszutphen/"><img class="sidebar-icon" src="img/icons/bau-instagram.svg" alt=""></a></li>
                        <li><a href="https://twitter.com/baudartius?lang=nl"><img class="sidebar-icon" src="img/icons/bau-twitter.svg" alt=""></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
