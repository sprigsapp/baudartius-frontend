
</div><!-- /.main-content -->

<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script src="js/vendor/jquery-ui.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/isotope.pkgd.min.js"></script>
<script src="js/vendor/imagesloaded.pkgd.min.js"></script>
<script src="js/vendor/jquery.fancybox.js"></script>
<script src="js/vendor/jquery.fancybox-thumbs.js"></script>

<script src="js/vendor/moment.min.js"></script>
<script src="js/vendor/fullcalendar.js"></script>
<script src="js/vendor/lang-all.js"></script>

<script src="js/main.js"></script>
<script src="js/ajax.js"></script>
</body>
</html>
