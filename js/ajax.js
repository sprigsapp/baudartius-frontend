//  Load more Ajax

var container = jQuery("#masonry-grid");
var loadBtn = $('#load-more-news');


//Infinite Scroll
$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        $('#load-more-news').click();
    }
});


//Load More News

$('#load-more-news').on('click', function () {
    var self = jQuery(this);
    var url = 'ajax/news.html';
    var loadanim = jQuery('#loader');
    loadanim.show();
    var itemLoad = 4;
    jQuery.ajax({
        url: url,
        data: {
            itemCount: itemLoad
        }
    }).done(function(data) {
        container.isotope('insert', jQuery(data));
        container.isotope('insert', jQuery(data.content)).imagesLoaded( function() {
            container.isotope('layout');
            loadanim.hide();

        });

        self.show();
    });
});


