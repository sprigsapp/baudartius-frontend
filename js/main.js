//Gallerie
function gallerie() {
    $('.fancybox').fancybox({
        padding: 0,
        arrows: true,
        helpers: {
            thumbs: true
        },

    });

};

// Reset Nav
function resetNav() {

    var subNav = $('ul.sub-menu');
    subNav.css('display', 'none');

};

//Main Navigation
function mainNavigation() {
    var mainHamburgerWrapper = $('.main-navigation-wrapper');
    var hamburgerIcon = $('.nav');
    var bodyOverlay = $('.body-overlay');
    hamburgerIcon.on('click', function () {
        var $self = $(this);
        if (hamburgerIcon.hasClass('open')) {
            $self.removeClass('open');
            mainHamburgerWrapper.removeClass('nav-open');
            bodyOverlay.fadeOut(400).stop(true, true);
            resetNav();

        } else {
            $self.addClass('open');
            mainHamburgerWrapper.addClass('nav-open');
            bodyOverlay.fadeIn(400).stop(true, true);
            //$self.next('.icon-subnavigation').addClass('rotate');


        }
    });


    bodyOverlay.on('click', function () {

        if (hamburgerIcon.hasClass('open')) {
            hamburgerIcon.removeClass('open');
            mainHamburgerWrapper.removeClass('nav-open');
            bodyOverlay.fadeOut(400);
            resetNav();
        } else {
            hamburgerIcon.addClass('open');
            mainHamburgerWrapper.addClass('nav-open');
            bodyOverlay.fadeIn(400);
        }
    });


};

function blogPost() {
    $('.blog-post').each(function (index, el) {
        var $el = $(el);
        var imageHeight = $el.find('.feature-image > img').height();
        var blogContent = $el.find('.blog-content');

        if ($el.find('.feature-image > img').length > 0) {
            blogContent.css('margin-top', (imageHeight / -2));
        }

    });
}


$(document).ready(function ($) {

    // googleMaps();
    mainNavigation();


    var currentPage = $('.current-menu-item').parents('.sub-menu');
    currentPage.show();

    $('.current-menu-parent').addClass('is-active');
    $('#trigger-open').on('click', function () {
        $('#search-form').toggleClass('active');
    });


    // Toggle Sub Nav

    //(".menu-item-has-children")
    $(".menu-item-has-children > a").attr('href', '#');

    var menuItem = '.menu > .menu-item-has-children';
    var submenuItem = '.sub-menu > .menu-item-has-children';
    var submenuContainer = '.menu > li > .sub-menu';
    var subSubmenuContainer = '.sub-menu > li > .sub-menu';

    function toggleSubmenu($this, menuItems, submenuContainers) {
        var thisMenuItem = $this.parent(),
            thisSubmenuItem = thisMenuItem.children('.sub-menu');

        if (thisMenuItem.hasClass('is-active')) {
            thisSubmenuItem.slideUp(200);
            thisMenuItem.removeClass('is-active');
        } else {
            $(menuItems).removeClass('is-active');
            $(submenuContainers).slideUp();
            thisSubmenuItem.slideDown(200);
            thisMenuItem.addClass('is-active');
        }
    }


    $(document).on('click', menuItem + ' > a', function (e) {
        var $this = $(this);
        toggleSubmenu($this, menuItem, submenuContainer);
    });

    $(document).on('click', submenuItem + '> a', function (e) {
        var $this = $(this);
        toggleSubmenu($this, submenuItem, subSubmenuContainer);
    });


    $('.btn-share').on('click', function () {
        var $shareLinks = $('.share-buttons');
        $shareLinks.fadeToggle(100);
    });


    var socialIcons = $('#social-icons');

    $('#trigger-social').on('click', function () {
        $('#sub-webmail').hide();
        socialIcons.fadeToggle();
    });

    var webMailIcons = $('#sub-webmail');
    $('#trigger-webmail').on('click', function () {
      $('#social-icons').hide();
        webMailIcons.fadeToggle();
    });


    $(document).mouseup(function (e)
    {
        var sidebar = $("#sub-webmail, #social-icons");
        if (!sidebar.is(e.target) // if the target of the click isn't the sidebar...
            && sidebar.has(e.target).length === 0) // ... nor a descendant of the sidebar
        {
            sidebar.hide();
        }
    });



});

$(window).load(function () {
    $('.sub-menu').closest('.menu-item-has-children').prepend('<i class="icon-subnavigation"></i>');
    blogPost();
});

jQuery(window).load(function ($) {
    var container = jQuery("#masonry-grid");
    if (container.length > 0) {
        container.isotope({
            layoutMode: 'masonry',
            // itemSelector: '.mix',
            transitionDuration: '0.7s',
            columnWidth: 60,
        });
    }

    gallerie();
});

jQuery(window).resize(function () {
    blogPost();
});
