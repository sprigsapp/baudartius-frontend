<?php  include('partials/header.php'); ?>


<div class="container">
    <div class="row">

        <div class="col-md-4 col-sm-4 col-xs-12 gallery-thumbnail">
            <div class="years-wrap">
                <div class="years-box">
                    <h2>2015</h2>
                    <h2>2016</h2>
                </div>
            </div>
            <div class="album-description">
                <h3>TIK OF KLIK OP EEN ALBUM ></h3>
            </div>
        </div><!--end of year box-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox" rel="gallery-1" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>MUZIEKAVOND</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-1" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-1" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-1" href="images/gallery/1/4.png"></a>
                </div>
            </div>
        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-2" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-2" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-2" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-2" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-3" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-3" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-3" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-3" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-4" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-4" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-4" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-4" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-5" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-5" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-5" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-5" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-12 gallery-thumbnail">
            <div class="years-wrap">
                <div class="years-box">
                    <h2>2013</h2>
                    <h2>2014</h2>
                </div>
            </div>
            <div class="album-description">
                <h3>TIK OF KLIK OP EEN ALBUM ></h3>
            </div>
        </div><!--end of year box-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox" rel="gallery-6" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>MUZIEKAVOND</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-6" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-6" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-6" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>
        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-7" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-7" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-7" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-7" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-8" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-8" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-8" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-8" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-9" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-9" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-9" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-9" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

        <div class="col-md-4 col-sm-4 col-xs-6 gallery-thumbnail">
            <div class="gallery">
                <a class="fancybox feature-image" rel="gallery-10" data-thumbnail="images/gallery/1/1.png" href="images/gallery/1/1.png">
                    <img src="images/gallery/1/img_thumbnail.jpg"/><!-- 400x400-->
                    <div class="album-description">
                        <h3>STUDIEDAG</h3>
                    </div>
                </a>

                <div class="group-gallery">
                    <a class="fancybox" rel="gallery-10" data-thumbnail="images/gallery/1/4.png" href="images/gallery/1/2.png"></a>
                    <a class="fancybox" rel="gallery-10" data-thumbnail="images/gallery/1/3.png" href="images/gallery/1/3.png"></a>
                    <a class="fancybox" rel="gallery-10" data-thumbnail="images/gallery/1/2.png" href="images/gallery/1/4.png"></a>
                </div>
            </div>

        </div><!-- End of Album-->

    </div>
</div>



<?php  include('partials/footer.php'); ?>