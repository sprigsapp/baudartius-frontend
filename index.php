 <?php include('partials/header.php'); ?>


 <div class="container">
             <div class="js-masonry" id="masonry-grid">
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_1.jpg" alt="">
                                 <div class="item-content">
                                     <h4>onze leerling</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_2.jpg" alt="">
                                 <div class="item-content">
                                     <h4>At vero eos et accusamus et iusto odio dignissimos ducimus.</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_3.jpg" alt="">
                                 <div class="item-content">
                                     <h4>onze collega</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_4.jpg" alt="">
                                 <div class="item-content">
                                     <h4>wie is Billy?</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-8 col-sm-8 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item iframe">
                             <iframe src="https://www.youtube.com/embed/ubX-nx8o3vs" frameborder="0" allowfullscreen
                                     webkitallowfullscreen></iframe>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_5.png" alt="">
                                 <div class="item-content">
                                     <h4>onze APP</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                     </div>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-12 mix" >
                     <div class="row">
                         <div class="masonry-item">
                             <a href="#">
                                 <img src="images/home/img_6.jpg" alt="">
                                 <div class="item-content">
                                     <h4>onze APP</h4>
                                 </div>
                             </a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
 </div>


 <?php  include('partials/footer.php'); ?>