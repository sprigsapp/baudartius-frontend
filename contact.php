<?php include('partials/header.php'); ?>


<div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 mix contact-box">
                <div class="masonry-item">
                    <div class="image-holder">
                        <img src="images/contact/img_1.jpg" alt="">
                        <a href="contact-single.php" class="btn-more"><i class="icon-arrow-right"></i>Meer</a>
                    </div>
                    <div class="item-description">
                        <h3>HOOFDLOKATIE ISENDOORSTRAAT</h3>
                        <p>Klas 3 en hoger</p>
                        <br>
                        <p>Isendoornstraat 1 <br>
                            7201 NJ Zutphen <br>
                            0575-515041</p>
                    </div>

                    <div id="map-1" class="map"></div>
                    <script>
                        var map;
                        map = new GMaps({
                            div: '#map-1',
                            lat: -12.043333,
                            lng: -77.028333
                        });
                        map.addMarker({
                            lat: -12.043333,
                            lng: -77.03,
                            title: 'Lima'
                        });
                    </script>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mix contact-box">
                <div class="masonry-item">
                    <div class="image-holder">
                        <img src="images/contact/img_1.jpg" alt="">
                    </div>
                    <div class="item-description">
                        <h3>HOOFDLOKATIE ISENDOORSTRAAT</h3>
                        <p>Klas 3 en hoger</p>
                        <br>
                        <p>Isendoornstraat 1 <br>
                            7201 NJ Zutphen <br>
                            0575-515041</p>
                    </div>

                    <div id="map-2" class="map"></div>
                    <script>
                        var map;
                        map = new GMaps({
                            div: '#map-2',
                            lat: -12.043333,
                            lng: -77.028333
                        });
                        map.addMarker({
                            lat: -12.043333,
                            lng: -77.03,
                            title: 'Lima'
                        });
                    </script>

                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mix contact-box">
                <div class="masonry-item">
                    <div class="image-holder">
                        <img src="img/envelope.png" alt="">
                        <a href="contact-single.php" class="btn-more"><i class="icon-arrow-right"></i>Meer</a>
                    </div>
                    <div class="item-description">
                        <h3>E-MAIL ADRESSEN</h3>
                        <p>Direct mailcontact met <br>
                        medewerkers van het <br>
                        Baudartius College</p>
                    </div>

                </div>
            </div>
        </div>
</div>




<?php include('partials/footer.php'); ?>