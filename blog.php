<?php include('partials/header.php'); ?>

<div class="container">
    <div class="blog-post-wrapper">

        <div class="blog-post">
            <div class="feature-image">
                <img src="images/blog/img_1.png" alt="">
                <a href="#" class="btn-more visible-xs"><i class="icon-arrow-right"></i>Meer</a>
            </div>
            <div class="blog-content col-md-8 col-md-offset-2 col-xs-12">
                <a href="#"><h3>SPORTDAG GROOT SUCCES</h3></a>
                <p>
                    Oluptatur sumque pro int que venem quis natur as aut accus et qui cus dolupti corit, tem autempos velessi odi simos velibus quo voluptur sernati isimporrore, quiduci imeniendem corpor aut quam aut ex experovid quas es volor aut ab ipitatur aut quam, vento eaquid velessi odi simos velibuscorpor aut quam auquas voloremperum cus ea verum resti omnient et.
                </p>

                <p>
                    <i>FOTO: nonsed que volora quo cum quae dollautatis molest quas atiis explam.</i>
                </p>

                <a href="#" class="btn-more hidden-xs"><i class="icon-arrow-right"></i>Meer</a>
            </div>
        </div><!--/.blog-post-->

        <div class="blog-post">
            <div class="blog-content col-md-8 col-md-offset-2 col-xs-12">
                <a href="#"><h3>SPORTDAG GROOT SUCCES</h3></a>
                <p>
                    Oluptatur sumque pro int que venem quis natur as aut accus et qui cus dolupti corit, tem autempos velessi odi simos velibus quo voluptur sernati isimporrore, quiduci imeniendem corpor aut quam aut ex experovid quas es volor aut ab ipitatur aut quam, vento eaquid velessi odi simos velibuscorpor aut quam auquas voloremperum cus ea verum resti omnient et.
                </p>

                <p>
                    <i>FOTO: nonsed que volora quo cum quae dollautatis molest quas atiis explam.</i>
                </p>
                <a href="#" class="btn-more"><i class="icon-arrow-right"></i>Meer</a>
            </div>
        </div><!--/.blog-post-->

        <div class="blog-post">
            <div class="feature-image">
                <img src="images/blog/img_1.png" alt="">
                <a href="#" class="btn-more"><i class="icon-arrow-right"></i>Meer</a>
            </div>
            <div class="blog-content col-md-8 col-md-offset-2 col-xs-12">
                <a href="#"><h3>SPORTDAG GROOT SUCCES</h3></a>
                <p>
                    Oluptatur sumque pro int que venem quis natur as aut accus et qui cus dolupti corit, tem autempos velessi odi simos velibus quo voluptur sernati isimporrore, quiduci imeniendem corpor aut quam aut ex experovid quas es volor aut ab ipitatur aut quam, vento eaquid velessi odi simos velibuscorpor aut quam auquas voloremperum cus ea verum resti omnient et.
                </p>

                <p>
                    <i>FOTO: nonsed que volora quo cum quae dollautatis molest quas atiis explam.</i>
                </p>
            </div>
        </div><!--/.blog-post-->

    </div>
</div>

<?php include('partials/footer.php'); ?>