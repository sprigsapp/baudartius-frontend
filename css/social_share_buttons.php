<?php $share_count = get_url_shares_count(get_the_permalink()); ?>
<ul>
	<li>
		<span class="share">
			<?php echo $share_count; ?> <?php _e('Shares', 'kosovo20') ?>
		</span>
	</li>

	<li>
		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank"><i class="fa fa-facebook"></i></a>
	</li>
	<li>
		<a href="https://twitter.com/share?url=<?php echo site_url('?p='.get_the_ID()) ?>&text=<?php the_title() ?>" target="_blank"><i class="fa fa-twitter"></i></a>
	</li>
	<li>
		<a href="mailto:?body=<?php the_permalink() ?>"><i class="fa fa-envelope-o"></i></a>
	</li>
	<?php if (comments_open() && get_field('article_type') == 'default'): ?>
		<li>
			<a href="#">
				<i class="fa fa-comment-o"></i>
				<span class="count-comments"><?php echo get_comments_number(); ?></span>
			</a>
		</li>
	<?php endif ?>
</ul>