<?php include('partials/header.php'); ?>

<div class="container">
    <article class="contact-content">
        <h1>Baudartius College Zutphen</h1>

        <h3>Postadres</h3>
        <p>Postbus 4018</p>
        <p>7200 BA Zutphen</p>
        <br>

        <h3>Rekeningnummer</h3>
        <p>NL 12 RABO 0376 7820 80</p>
        <p>(t.n.v. Vereniging Christelijk Voortgezet Onderwijs te Zutphen)</p>
        <br>

        <h3>Vereniging Christelijk Voortgezet Onderwijs te Zutphen</h3>
        <p>Kamer van Koophandel: 40101542</p>
        <p>Fiscaal nummer: 27.03.518</p>

    </article>
</div>




<?php include('partials/footer.php'); ?>